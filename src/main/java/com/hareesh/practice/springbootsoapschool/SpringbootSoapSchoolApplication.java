package com.hareesh.practice.springbootsoapschool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSoapSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootSoapSchoolApplication.class, args);
	}

}
